# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '3.0.0'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 6.1.2', '>= 6.1.2.1'
# Use mysql as the database for Active Record
gem 'mysql2', '~> 0.5'
# Use Puma as the app server
gem 'puma', '~> 5.0'
# Ruby static code analyzer
gem 'rubocop', require: false
# Ruby static code analyzer
gem 'rubocop-rails', require: false
# Use SCSS for stylesheets
gem 'sass-rails', '>= 6'
# Transpile app-like JavaScript. Read more: https://github.com/rails/webpacker
gem 'webpacker', '~> 5.0'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.7'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use Active Model has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Active Storage variant
# gem 'image_processing', '~> 1.2'

# Using for state mashine
gem 'aasm'
# Using for abmin psnel
gem 'administrate'
# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.4.4', require: false
# Gem for adding styles
gem 'bootstrap', '~> 4.2.1'
gem 'bootstrap-email'
# Gem for authorization and registration
gem 'devise'
# Using fir generating fake data
gem 'faker'
# Using for coordinates and addresses
gem 'geocoder'
# Haml is a templating engine for HTML.
gem 'haml'
gem 'haml-rails'
# For good working bootstrap with jquery
gem 'jquery-rails'
gem 'mimemagic', '~> 0.4.2'
# Gem for debuging
gem 'pry'
# rails_best_practices is a code metric tool to check the quality of Rails code.
gem 'rails_best_practices'
# Gem for forms
gem 'simple_form'
# Using for generating PDF recipt
gem 'wicked_pdf'
gem 'wkhtmltopdf-binary'

group :development, :test do
  # Use for pritier output console
  gem 'amazing_print'
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  # Storing configuration in the environment is one of the tenets of a twelve-factor app.
  gem 'dotenv-rails'
  # Use rspec for testing
  gem "factory_bot_rails"
  # Is a fixtures replacement with a straightforward definition syntax
  gem 'factory_bot'
  gem 'rspec-rails', '~> 4.0.2'
  gem 'rubocop-rspec', require: false
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 4.1.0'
  # Display performance information such as SQL time and flame graphs for each request in your browser.
  # Can be configured to work on production as well see: https://github.com/MiniProfiler/rack-mini-profiler/blob/master/README.md
  gem 'rack-mini-profiler', '~> 2.0'
  # It watches your files as you change them
  gem 'listen', '~> 3.3'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
end

group :test do
  # Use for cleaning data in db
  gem 'database_cleaner-active_record'
  # Use for testing controllers
  gem 'rails-controller-testing'
  # Use for displaying covering of code
  gem 'simplecov', require: false, group: :test
  # Use for shorted tests
  gem 'shoulda-matchers'
end

group :production do
  gem 'pg'
  gem 'rails_12factor', '0.0.2'
  gem 'wkhtmltopdf-heroku'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
