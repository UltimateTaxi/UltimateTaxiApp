require 'rails_helper'

RSpec.describe Users::RegistrationsController, type: :controller do
  describe "POST #registrations" do
    let!(:user) do
      FactoryBot.build(:passenger).attributes
                .merge({ password: '123456', confirm_password: '123456' })
    end

    before do
      request.env['devise.mapping'] = Devise.mappings[:user]
    end

    context "with valid user params" do
      it "creates a new User" do
        expect do
          post :create, params: { user: user }
        end.to change(User, :count).by(1)
        expect(User.last.type).to eq("Passenger")
      end
    end
  end
end
