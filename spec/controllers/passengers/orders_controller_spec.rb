require 'rails_helper'
RSpec.describe Passengers::OrdersController, type: :controller do
  let!(:passenger)  { FactoryBot.create(:passenger) }
  let!(:tariff)     { FactoryBot.create(:tariff) }
  let!(:driver)     { FactoryBot.create(:driver, tariff_id: tariff.id) }
  let!(:order) do
    FactoryBot.create(:order, :created_state, passenger_id: passenger.id, tariff_id: tariff.id,
                                              driver_id: driver.id)
  end

  before { sign_in passenger }

  describe '#index' do
    subject { get :index }

    it { is_expected.to be_successful }
  end

  describe 'GET#show' do
    before { get :show, params: { id: order.id } }

    it { is_expected.to respond_with :success }
    it { is_expected.to render_template :show }

    it 'response status 200 format is pdf' do
      get :show, params: { id: order.id, format: :pdf }
      expect(response).to have_http_status(:success)
      expect(response.media_type).to eq 'application/pdf'
    end
  end

  describe 'GET#new' do
    it 'returns success and assigns order' do
      get :new
      expect(response).to have_http_status(:success)
      expect(assigns(:order)).to be_a_new(Order)
    end
  end
end
