require 'rails_helper'
RSpec.describe Drivers::OrdersController, type: :controller do
  let!(:passenger)  { FactoryBot.create(:passenger) }
  let!(:tariff)     { FactoryBot.create(:tariff) }
  let!(:driver)     { FactoryBot.create(:driver, tariff_id: tariff.id) }
  let!(:order) do
    FactoryBot.create(:order, passenger_id: passenger.id, tariff_id: tariff.id,
                              driver_id: driver.id,       state: Order::CREATED_STATE)
  end
  let!(:valid_params) do
    FactoryBot.attributes_for :order, tariff_id: tariff.id, passenger_id: passenger.id,
                                      driver_id: driver.id
  end

  before { sign_in driver }

  describe '#index' do
    subject { get :index }

    it { is_expected.to be_successful }
  end
end
