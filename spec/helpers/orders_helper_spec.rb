require 'spec_helper'
require 'rails_helper'

RSpec.describe OrdersHelper do
  describe 'get_human_readable_state' do
    it 'returns human readable Created state of order' do
      expect(helper.get_human_readable_state(Order::CREATED_STATE)).to eq("Created")
      expect(helper.get_human_readable_state(Order::ACCEPTED_STATE)).to eq("Accepted")
      expect(helper.get_human_readable_state(Order::WAITING_PASSENGER_STATE)).to eq("Waiting passenger")
      expect(helper.get_human_readable_state(Order::MOVING_STATE)).to eq("Moving")
      expect(helper.get_human_readable_state(Order::FINISHED_STATE)).to eq("Finished")
    end
  end
end
