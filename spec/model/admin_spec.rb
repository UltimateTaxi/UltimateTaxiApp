require 'rails_helper'

RSpec.describe Admin, type: :model do
  let!(:admin)     { FactoryBot.create(:admin) }
  let!(:tariff)    { FactoryBot.create(:tariff) }
  let!(:passenger) { FactoryBot.create(:passenger) }
  let!(:driver)    { FactoryBot.create(:driver, tariff_id: tariff.id) }
  let!(:order_this_month) do
    FactoryBot.create(:order, :this_month,  passenger_id: passenger.id, tariff_id: tariff.id,
                                            driver_id: driver.id, price: '100')
  end
  let!(:order_last_month) do
    FactoryBot.create(:order, :last_month,  passenger_id: passenger.id, tariff_id: tariff.id,
                                            driver_id: driver.id, price: '200')
  end
  let!(:order_older) do
    FactoryBot.create(:order, :older_month, passenger_id: passenger.id, tariff_id: tariff.id,
                                            driver_id: driver.id, price: '300')
  end

  context "when valid Factory" do
    it "has a valid factory" do
      expect(FactoryBot.build(:admin)).to be_valid
    end
  end
end
