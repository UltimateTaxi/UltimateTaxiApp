require 'rails_helper'

RSpec.describe Tariff, type: :model do
  let!(:tariff) { FactoryBot.build(:tariff) }

  context "when valid Factory" do
    it "has a valid factory" do
      expect(FactoryBot.build(:tariff)).to be_valid
    end
  end

  describe "Presence" do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:cost) }
  end

  describe "Length is invalid" do
    it { is_expected.not_to allow_value("a" * 2).for(:name) }
    it { is_expected.not_to allow_value("a" * 31).for(:name) }
  end

  describe "Length is valid" do
    it { is_expected.to allow_value("a" * 15).for(:name) }
  end
end
