require 'rails_helper'

RSpec.describe User, type: :model do
  let!(:tariff)    { FactoryBot.create(:tariff) }
  let!(:admin)     { FactoryBot.build(:admin) }
  let!(:passenger) { FactoryBot.build(:passenger) }

  before do
    allow(UserMailer).to receive_message_chain(:send_welcome_email, :deliver_now).and_return(true)
  end

  context "when valid Factory" do
    it "has a valid factory" do
      expect(FactoryBot.build(:admin)).to be_valid
      expect(FactoryBot.build(:passenger)).to be_valid
    end
  end

  describe "Presence" do
    it { is_expected.to validate_presence_of(:first_name) }
    it { is_expected.to validate_presence_of(:last_name) }
    it { is_expected.to validate_presence_of(:email) }
    it { is_expected.to validate_presence_of(:phone) }
    it { is_expected.to validate_presence_of(:type) }
    it { is_expected.to validate_presence_of(:password) }
  end

  describe "length is too short" do
    it { is_expected.not_to allow_value("a" * 1).for(:first_name) }
    it { is_expected.not_to allow_value("a" * 1).for(:last_name) }
    it { is_expected.not_to allow_value("a" * 4).for(:email) }
    it { is_expected.not_to allow_value("1" * 9).for(:phone) }
    it { is_expected.not_to allow_value("a" * 2).for(:type) }
    it { is_expected.not_to allow_value("a" * 4).for(:password) }
  end

  describe "length is too long" do
    it { is_expected.not_to allow_value("a" *  51).for(:first_name) }
    it { is_expected.not_to allow_value("a" *  51).for(:last_name) }
    it { is_expected.not_to allow_value("1" *  14).for(:phone) }
    it { is_expected.not_to allow_value("a" *  16).for(:type) }
    it { is_expected.not_to allow_value("a" *  31).for(:password) }
    it { is_expected.not_to allow_value("a" * 101).for(:email) }
  end

  describe "length is valid" do
    it { is_expected.to allow_value("a"    * 10).for(:first_name) }
    it { is_expected.to allow_value("a"    * 15).for(:last_name) }
    it { is_expected.to allow_value("1"    * 10).for(:phone) }
    it { is_expected.to allow_value("a"    * 11).for(:type) }
    it { is_expected.to allow_value("a"    * 10).for(:password) }
    it { is_expected.to allow_value("#{'a' * 88}@example.com").for(:email) }
  end

  describe ".send_welcome_email_to new_user" do
    let(:user) { FactoryBot.create(:passenger) }

    it 'calls UserMailer.send_welcome_email' do
      expect(UserMailer).to receive_message_chain(:send_welcome_email, :deliver_now)
      user.send_welcome_email_to_new_user
    end
  end

  describe 'admin?' do
    context 'when user is admin' do
      let(:admin) { FactoryBot.create(:admin) }

      it 'returns true if user is admin' do
        expect(admin.admin?).to be_truthy
      end
    end
  end

  describe 'driver?' do
    context 'when user is driver' do
      let(:tariff) { FactoryBot.create(:tariff) }
      let(:driver) { FactoryBot.create(:driver, tariff_id: tariff.id) }

      it 'returns true if user is driver' do
        expect(driver.driver?).to be_truthy
      end
    end
  end

  describe 'passenger?' do
    context 'when user is passenger' do
      let(:passenger) { FactoryBot.create(:passenger) }

      it 'returns true if user is passenger' do
        expect(passenger.passenger?).to be_truthy
      end
    end
  end
end
