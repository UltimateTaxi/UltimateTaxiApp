require 'rails_helper'

RSpec.describe Order, type: :model do
  let!(:admin)      { FactoryBot.create(:admin) }
  let!(:tariff)     { FactoryBot.create(:tariff) }
  let!(:passenger)  { FactoryBot.create(:passenger) }
  let!(:driver)     { FactoryBot.create(:driver, tariff_id: tariff.id) }
  let!(:order) do
    FactoryBot.create(:order, :created_state, passenger_id: passenger.id, tariff_id: tariff.id,
                                              driver_id: driver.id,       price: '400')
  end
  let!(:order_this_month) do
    FactoryBot.create(:order, :this_month,    passenger_id: passenger.id, tariff_id: tariff.id,
                                              driver_id: driver.id,       price: '100')
  end
  let!(:order_last_month) do
    FactoryBot.create(:order, :last_month,    passenger_id: passenger.id, tariff_id: tariff.id,
                                              driver_id: driver.id,       price: '200')
  end
  let!(:order_older) do
    FactoryBot.create(:order, :older_month,   passenger_id: passenger.id, tariff_id: tariff.id,
                                              driver_id: driver.id,       price: '300')
  end

  context "when valid Factory" do
    it "has a valid factory" do
      expect(FactoryBot.build(:order, passenger_id: passenger.id, tariff_id: tariff.id)).to be_valid
    end
  end

  describe ".send_order_finished_email when order is finished" do
    before do
      allow(OrderMailer).to receive_message_chain(:send_order_finished_email, :deliver_now).and_return(true)
    end

    it 'calls OrderMailer.send_order_finished_email_to_passenger' do
      expect(OrderMailer).to receive_message_chain(:send_order_finished_email, :deliver_now)
      order.send(:send_order_finished_email_to_passenger)
    end
  end

  describe ".change_status" do
    context ".accept!" do
      let!(:order) do
        FactoryBot.create(:order, :created_state, passenger_id: passenger.id, tariff_id: tariff.id,
                                                  driver_id: driver.id)
      end

      before do
        allow(order).to receive(:accept!).and_return(true)
      end

      it 'calls to change_order accept!' do
        order.accept!
        expect(order.change_status).to be_truthy
      end
    end

    context ".wait_passenger!" do
      let!(:order) do
        FactoryBot.create(:order, :accepted_state, passenger_id: passenger.id, tariff_id: tariff.id,
                                                   driver_id: driver.id)
      end

      before do
        allow(order).to receive(:wait_passenger!).and_return(true)
      end

      it 'calls to change_order wait_passenger!' do
        order.wait_passenger!
        expect(order.change_status).to be_truthy
      end
    end

    context ".move!" do
      let!(:order) do
        FactoryBot.create(:order, :waiting_pasenger_state, passenger_id: passenger.id, tariff_id: tariff.id,
                                                           driver_id: driver.id)
      end

      before do
        allow(order).to receive(:move!).and_return(true)
      end

      it 'calls to change_order move!' do
        order.move!
        expect(order.change_status).to be_truthy
      end
    end

    context ".finish!" do
      let!(:order) do
        FactoryBot.create(:order, :moving_state, passenger_id: passenger.id, tariff_id: tariff.id,
                                                 driver_id: driver.id)
      end

      before do
        allow(order).to receive(:finish!).and_return(true)
      end

      it 'calls to change_order finish!' do
        order.finish!
        expect(order.change_status).to be_truthy
      end
    end
  end

  # Total calculating

  describe "#total_price" do
    context "when orders exist" do
      it 'returns total sum of price' do
        expect(described_class.total_price).to eq({ driver.id => 1000 })
      end
    end
  end

  describe "#total_prices_fee" do
    context "when orders exist" do
      it 'returns total prices fee' do
        expect(described_class.total_prices_fee).to eq({ driver.id => 100 })
      end
    end
  end

  describe "#total_price_salary" do
    context "when orders exist" do
      it 'returns total prices salary' do
        expect(described_class.total_price_salary).to eq({ driver.id => 900 })
      end
    end
  end

  # Calculating for this month

  describe "#price_per_this_month" do
    context "when orders exist" do
      it 'returns sum per this month' do
        expect(described_class.price_per_this_month).to eq({ driver.id => 500 })
      end
    end
  end

  describe "#fee_per_this_month" do
    context "when orders exist" do
      it 'returns fee per this month' do
        expect(described_class.fee_per_this_month).to eq({ driver.id => 50.to_d })
      end
    end
  end

  describe "#salary_per_this_month" do
    context "when orders exist" do
      it 'returns salary per this month' do
        expect(described_class.salary_per_this_month).to eq({ driver.id => 450 })
      end
    end
  end

  # Calculating for last month

  describe "#price_per_last_month" do
    context "when orders exist" do
      it 'returns sum per last month' do
        expect(described_class.price_per_last_month).to eq({ driver.id => 200 })
      end
    end
  end

  describe "#fee_per_last_month" do
    context "when orders exist" do
      it 'returns fee per last month' do
        expect(described_class.fee_per_last_month).to eq({ driver.id => 20.0 })
      end
    end
  end

  describe "#salary_per_last_month" do
    context "when orders exist" do
      it 'returns salary per last month' do
        expect(described_class.salary_per_last_month).to eq({ driver.id => 180.0 })
      end
    end
  end
end
