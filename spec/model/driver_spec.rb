require 'rails_helper'

RSpec.describe Driver, type: :model do
  let!(:tariff) { FactoryBot.create(:tariff) }
  let!(:driver) { FactoryBot.build(:driver, tariff_id: tariff.id) }

  context "when valid Factory" do
    it "has a valid factory" do
      expect(FactoryBot.build(:driver, tariff_id: tariff.id)).to be_valid
    end
  end

  describe "Presence" do
    it { is_expected.to validate_presence_of(:first_name) }
    it { is_expected.to validate_presence_of(:last_name) }
    it { is_expected.to validate_presence_of(:email) }
    it { is_expected.to validate_presence_of(:phone) }
    it { is_expected.to validate_presence_of(:type) }
  end

  describe "length is too short" do
    it { is_expected.not_to allow_value("a" * 1).for(:first_name) }
    it { is_expected.not_to allow_value("a" * 1).for(:last_name) }
    it { is_expected.not_to allow_value("a" * 4).for(:email) }
    it { is_expected.not_to allow_value("1" * 9).for(:phone) }
    it { is_expected.not_to allow_value("a" * 2).for(:type) }
  end

  describe "length is too long" do
    it { is_expected.not_to allow_value("a" *  51).for(:first_name) }
    it { is_expected.not_to allow_value("a" *  51).for(:last_name) }
    it { is_expected.not_to allow_value("1" *  14).for(:phone) }
    it { is_expected.not_to allow_value("a" *  16).for(:type) }
    it { is_expected.not_to allow_value("a" * 101).for(:email) }
  end

  describe "length is valid" do
    it { is_expected.to allow_value("a" *    10).for(:first_name) }
    it { is_expected.to allow_value("a" *    15).for(:last_name) }
    it { is_expected.to allow_value("1" *    10).for(:phone) }
    it { is_expected.to allow_value("a" *    11).for(:type) }
    it { is_expected.to allow_value(1..4).for(:tariff_id) }
    it { is_expected.to allow_value("#{'a' * 88}@example.com").for(:email) }
  end

  describe ".price_per_this_month_for_driver" do
    let!(:tariff)    { FactoryBot.create(:tariff) }
    let!(:passenger) { FactoryBot.create(:passenger) }
    let!(:driver)    { FactoryBot.create(:driver, tariff_id: tariff.id) }

    context "when orders is in date range" do
      let!(:order) do
        FactoryBot.create(:order, passenger_id: passenger.id, tariff_id: tariff.id,
                                  driver_id: driver.id,       price: '145')
      end

      it 'returns sum of price per this month' do
        expect(driver.send(:price_per_this_month_for_driver)).to eq(order.price)
      end
    end

    context ".driver_salary" do
      let!(:order) do
        FactoryBot.create(:order, passenger_id: passenger.id, tariff_id: tariff.id,
                                  driver_id: driver.id,       price: '100')
      end

      it 'returns salary per this month' do
        expect(driver.driver_salary).to eq(90.0)
      end
    end

    context "when no orders in date range" do
      let!(:order) do
        FactoryBot.create(:order, :last_month, passenger_id: passenger.id, tariff_id: tariff.id,
                                               driver_id: driver.id,       price: '215')
      end
      it 'returns sum of price per this month' do
        expect(driver.send(:price_per_this_month_for_driver)).to eq(0)
      end
    end
  end
end
