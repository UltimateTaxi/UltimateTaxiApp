require 'spec_helper'
require 'rails_helper'

RSpec.describe CalculatePriceService, type: :service do
  let(:first_address) do
    "Заравшан, 21Б, Oleksandra Dovzhenka Street, Патріот, Городок, Ivano-Frankivsk,"\
    " Ivano-Frankivsk City Hromada, Ivano-Frankivsk Raion, Ivano-Frankivsk Oblast,"\
    " 7600, Ukraine,48.9026604,24.6885631"
  end
  let(:second_address) do
    "Tysiv, Bolekhiv City Hromada, Kalush Raion, Ivano-Frankivsk Oblast, Ukraine,49.048546,23.768635"
  end
  let!(:tariff) { FactoryBot.create(:tariff, cost: '10') }
  let(:expected_response) { 690.73 }

  describe "geocode" do
    it 'returns fee' do
      expect(CalculatePriceService.new(first_address, second_address, tariff.id).calculate).to eq(expected_response)
    end
  end
end
