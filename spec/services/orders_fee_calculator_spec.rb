require 'spec_helper'
require 'rails_helper'

RSpec.describe OrdersFeeCalculatorService, type: :service do
  let!(:tariff)     { FactoryBot.create(:tariff) }
  let!(:passenger)  { FactoryBot.create(:passenger) }
  let!(:driver)     { FactoryBot.create(:driver, id: 1, tariff_id: tariff.id) }
  let!(:order) do
    FactoryBot.create(:order, passenger_id: passenger.id, tariff_id: tariff.id,
                              driver_id: driver.id, price: '100')
  end
  let(:expected_response) { 10 }

  describe "calculate_fee" do
    it 'returns fee' do
      expect(OrdersFeeCalculatorService.calculate_fee(order.price)).to eq(expected_response)
    end
  end
end
