require 'spec_helper'
require 'rails_helper'

RSpec.describe AddressGeocoderService, type: :service do
  let(:expected_response) do
    [["Заравшан, 21Б, Oleksandra Dovzhenka Street, Патріот, Городок, Ivano-Frankivsk,"\
      " Ivano-Frankivsk City Hromada, Ivano-Frankivsk Raion, Ivano-Frankivsk Oblast,"\
      " 7600, Ukraine", [48.9026604, 24.6885631]]]
  end

  describe "geocode" do
    it 'returns fee' do
      expect(AddressGeocoderService.new("Заравшан").geocode).to eq(expected_response)
    end
  end
end
