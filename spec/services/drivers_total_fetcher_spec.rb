require 'spec_helper'
require 'rails_helper'

RSpec.describe DriversTotalFetcherService, type: :service do
  let!(:tariff)     { FactoryBot.create(:tariff) }
  let!(:passenger)  { FactoryBot.create(:passenger) }
  let!(:driver)     { FactoryBot.create(:driver, id: 1, tariff_id: tariff.id) }
  let!(:order) do
    FactoryBot.create(:order, passenger_id: passenger.id, tariff_id: tariff.id,
                              driver_id: driver.id, price: '400')
  end
  let(:expected_response) do
    { total_price: { 1 => 400 },
      price_per_this_month: { 1 => 400 },
      price_per_last_month: {},
      total_prices_fee: { 1 => 40.to_d },
      fee_per_this_month: { 1 => 40.to_d },
      fee_per_last_month: {},
      total_price_salary: { 1 => 360.to_d },
      salary_per_this_month: { 1 => 360.to_d },
      salary_per_last_month: {} }
  end

  describe "fetch" do
    it 'returns statistic from drivers total service' do
      expect(DriversTotalFetcherService.fetch).to eq(expected_response)
    end
  end
end
