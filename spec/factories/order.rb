FactoryBot.define do
  factory :order do
    driver_id { Faker::Number.between(from: 1, to: 100) }
    passenger_id { Faker::Number.between(from: 1, to: 100) }
    tariff_id { Faker::Number.between(from: 1, to: 4) }
    starting_address { Faker::Address.full_address }
    destination_address { Faker::Address.full_address }
    price { Faker::Number.decimal(l_digits: 2) }

    state do
      [Order::CREATED_STATE, Order::ACCEPTED_STATE, Order::WAITING_PASSENGER_STATE,\
       Order::MOVING_STATE, Order::FINISHED_STATE].sample
    end

    trait :this_month do
      created_at { Time.zone.now }
    end

    trait :last_month do
      created_at { Time.zone.now - 1.month }
    end

    trait :older_month do
      created_at { Time.zone.now - 2.months }
    end

    trait :created_state do
      state { Order::CREATED_STATE }
    end

    trait :accepted_state do
      state { Order::ACCEPTED_STATE }
    end

    trait :waiting_pasenger_state do
      state { Order::WAITING_PASSENGER_STATE }
    end

    trait :moving_state do
      state { Order::MOVING_STATE }
    end
  end
end
