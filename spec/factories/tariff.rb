FactoryBot.define do
  factory :tariff do
    name { %w[Econom Standart Business VIP].sample }
    cost { [7, 10, 15, 20].sample }
  end
end
