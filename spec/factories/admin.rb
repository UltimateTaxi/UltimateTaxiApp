FactoryBot.define do
  factory :admin do
    first_name { Faker::Name.first_name }
    last_name  { Faker::Name.last_name }
    email { Faker::Internet.email }
    password { Faker::Internet.password }
    phone { Faker::Number.leading_zero_number(digits: 10) }
    type { 'Admin' }
  end
end
