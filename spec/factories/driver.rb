FactoryBot.define do
  factory :driver do
    first_name { Faker::Name.first_name }
    last_name  { Faker::Name.last_name }
    email { Faker::Internet.email }
    password { Faker::Internet.password }
    phone { Faker::Number.leading_zero_number(digits: 10) }
    type { 'Driver' }
    tariff_id { Faker::Number.between(from: 1, to: 4) }
  end
end
