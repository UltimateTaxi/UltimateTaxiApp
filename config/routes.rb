Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  devise_for :users, controllers: { registrations: 'users/registrations' }

  root 'home#index', as: 'home'
  # get 'about', to: 'home#about'
  get 'tariff', to: 'home#tariff'

  namespace :admins do
    resources :drivers do
      collection do
        get :statistic
      end
    end

    resources :feedbacks
    resources :passengers
    resources :orders
    resources :tariffs

    root "orders#index"
  end

  namespace :passengers do
    resources :orders do
      resources :feedbacks, only: %i[create]

      get 'geocode_address', to: 'orders#geocode_address'
      get 'calculate_price', to: 'orders#calculate_price'
    end
  end

  namespace :drivers do
    resources :orders, except: %i[new create destroy] do
      resources :feedbacks, only: %i[create]

      member do
        get :change_order_status
      end
    end
  end
end
