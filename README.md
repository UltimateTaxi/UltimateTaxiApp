# Ultimate Taxi

### To run the project you need:

  [Ruby v.3.0.0](https://gorails.com/setup/ubuntu/20.10)

  [Rails v.6.1.1](https://gorails.com/setup/ubuntu/20.10#rails)

  MySQL Database

### Link on deployed project
  [Ultimate Taxi](https://ultimatetaxi.herokuapp.com/)
