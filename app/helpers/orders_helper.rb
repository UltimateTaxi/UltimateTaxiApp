module OrdersHelper
  def get_human_readable_state(state)
    case state
    when Order::CREATED_STATE
      "Created"
    when Order::ACCEPTED_STATE
      "Accepted"
    when Order::WAITING_PASSENGER_STATE
      "Waiting passenger"
    when Order::MOVING_STATE
      "Moving"
    else
      "Finished"
    end
  end
end
