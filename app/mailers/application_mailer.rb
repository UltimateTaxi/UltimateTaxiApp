class ApplicationMailer < ActionMailer::Base
  default from: 'noreply@ultaxi.com'
  layout 'bootstrap-mailer'
end
