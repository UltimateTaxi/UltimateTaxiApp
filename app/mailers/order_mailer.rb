class OrderMailer < ApplicationMailer
  def send_order_finished_email(order)
    @order = order
    mail(to: order.passenger.email, subject: 'Your order finished!')
  end
end
