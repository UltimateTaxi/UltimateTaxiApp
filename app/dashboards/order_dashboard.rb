require "administrate/base_dashboard"

class OrderDashboard < Administrate::BaseDashboard
  ATTRIBUTE_TYPES = {
    driver: Field::BelongsTo,
    passenger: Field::BelongsTo,
    tariff: Field::BelongsTo,
    id: Field::Number,
    starting_address: Field::String,
    destination_address: Field::String,
    price: Field::String.with_options(searchable: false),
    state: Field::String,
    created_at: Field::DateTime,
    updated_at: Field::DateTime
  }.freeze

  COLLECTION_ATTRIBUTES = %i[
    id
    driver
    passenger
    tariff
    price
    state
  ].freeze

  SHOW_PAGE_ATTRIBUTES = %i[
    driver
    passenger
    tariff
    id
    starting_address
    destination_address
    price
    state
    created_at
    updated_at
  ].freeze

  FORM_ATTRIBUTES = %i[
    driver
    passenger
    tariff
    starting_address
    destination_address
    price
    state
  ].freeze

  COLLECTION_FILTERS = {}.freeze
end
