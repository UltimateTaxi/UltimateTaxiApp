require "administrate/base_dashboard"

class TariffDashboard < Administrate::BaseDashboard
  ATTRIBUTE_TYPES = {
    drivers: Field::HasMany,
    orders: Field::HasMany,
    id: Field::Number,
    name: Field::String,
    cost: Field::String.with_options(searchable: false),
    created_at: Field::DateTime,
    updated_at: Field::DateTime
  }.freeze

  COLLECTION_ATTRIBUTES = %i[
    drivers
    orders
    id
    name
  ].freeze

  SHOW_PAGE_ATTRIBUTES = %i[
    id
    name
    cost
    created_at
    updated_at
    drivers
    orders
  ].freeze

  FORM_ATTRIBUTES = %i[
    name
    cost
  ].freeze

  COLLECTION_FILTERS = {}.freeze

  def display_resource(tariff)
    tariff.name
  end
end
