require "administrate/base_dashboard"

class FeedbackDashboard < Administrate::BaseDashboard
  ATTRIBUTE_TYPES = {
    passenger: Field::BelongsTo,
    driver: Field::BelongsTo,
    order: Field::BelongsTo,
    id: Field::Number,
    rating: Field::Number,
    text: Field::Text,
    created_at: Field::DateTime,
    updated_at: Field::DateTime
  }.freeze

  COLLECTION_ATTRIBUTES = %i[
    id
    order
    passenger
    driver
    rating
    text
  ].freeze

  SHOW_PAGE_ATTRIBUTES = %i[
    id
    order
    passenger
    driver
    rating
    text
    created_at
    updated_at
  ].freeze

  FORM_ATTRIBUTES = %i[
    order
    passenger
    driver
    rating
    text
  ].freeze

  COLLECTION_FILTERS = {}.freeze
end
