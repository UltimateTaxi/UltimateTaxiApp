require "administrate/base_dashboard"

class DriverDashboard < Administrate::BaseDashboard
  ATTRIBUTE_TYPES = {
    tariff: Field::BelongsTo,
    orders: Field::HasMany,
    id: Field::Number,
    email: Field::String,
    encrypted_password: Field::String,
    reset_password_token: Field::String,
    reset_password_sent_at: Field::DateTime,
    remember_created_at: Field::DateTime,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    first_name: Field::String,
    last_name: Field::String,
    type: Field::String,
    phone: Field::String,
    password: PasswordField,
    password_confirmation: PasswordField
  }.freeze

  COLLECTION_ATTRIBUTES = %i[
    tariff
    orders
    id
    email
    first_name
    last_name
    phone
    type
  ].freeze

  SHOW_PAGE_ATTRIBUTES = %i[
    tariff
    id
    email
    first_name
    last_name
    phone
    type
    created_at
    updated_at
    orders
  ].freeze

  FORM_ATTRIBUTES = %i[
    tariff
    email
    first_name
    last_name
    phone
    password
    password_confirmation
  ].freeze

  COLLECTION_FILTERS = {}.freeze

  def display_resource(driver)
    "#{driver.first_name} #{driver.last_name}"
  end
end
