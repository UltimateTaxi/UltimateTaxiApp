class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  validates :first_name, presence: true, length: 2..50
  validates :last_name, presence: true, length: 2..50
  validates :email, presence: true, format: { with: URI::MailTo::EMAIL_REGEXP }, uniqueness: true, length: 5..100
  validates :phone, presence: true, numericality: true, length: 10..13
  validates :type, presence: true, length: 3..15
  validates :password, presence: true, length: 6..30, confirmation: true

  after_create :send_welcome_email_to_new_user

  def send_welcome_email_to_new_user
    UserMailer.send_welcome_email(self).deliver_now
  end

  def admin?
    is_a?(Admin)
  end

  def driver?
    is_a?(Driver)
  end

  def passenger?
    is_a?(Passenger)
  end
end
