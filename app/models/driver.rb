class Driver < User
  belongs_to :tariff
  has_many :orders, dependent: :nullify
  has_many :feedbacks, dependent: :nullify

  def driver_salary
    (price_per_this_month_for_driver - price_per_this_month_for_driver * 0.1).to_d
  end

  private

  def price_per_this_month_for_driver
    Order.where("created_at >= ? AND created_at < ?",
                Time.zone.now.beginning_of_month, Time.zone.now.end_of_month)\
         .where(driver_id: id).sum(:price)
  end
end
