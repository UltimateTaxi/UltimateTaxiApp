class Tariff < ApplicationRecord
  has_many :drivers, dependent: :destroy
  has_many :orders, dependent: :nullify

  validates :name, presence: true, length: 3..30
  validates :cost, presence: true
end
