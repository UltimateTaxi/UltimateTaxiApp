class Order < ApplicationRecord
  CREATED_STATE           = 'created'.freeze
  ACCEPTED_STATE          = 'accepted'.freeze
  WAITING_PASSENGER_STATE = 'waiting_passenger'.freeze
  MOVING_STATE            = 'moving'.freeze
  FINISHED_STATE          = 'finished'.freeze

  ORDERS_FEE = 0.1

  has_many :feedbacks, dependent: :nullify

  belongs_to :driver, optional: true
  belongs_to :passenger
  belongs_to :tariff

  delegate :name, :cost, to: :tariff, prefix: true

  validates :starting_address, presence: true
  validates :destination_address, presence: true
  validates :price, presence: true, numericality: { greater_than: 0 }

  include AASM

  aasm column: 'state' do
    state :created, initial: true
    state :accepted
    state :waiting_passenger
    state :moving
    state :finished

    event :accept do
      transitions from: :created, to: :accepted
    end

    event :wait_passenger do
      transitions from: :accepted, to: :waiting_passenger
    end

    event :move do
      transitions from: :waiting_passenger, to: :moving
    end

    event :finish, after: :send_order_finished_email_to_passenger do
      transitions from: :moving, to: :finished
    end
  end

  def change_status
    case state
    when CREATED_STATE
      accept!
    when ACCEPTED_STATE
      wait_passenger!
    when WAITING_PASSENGER_STATE
      move!
    else
      finish!
    end
  end

  class << self
    # Total calculating

    def total_price
      Order.group(:driver_id).sum(:price)
    end

    def total_prices_fee
      total_price.transform_values { |sum| OrdersFeeCalculatorService.calculate_fee(sum) }
    end

    def total_price_salary
      total_price.transform_values { |sum| (sum - OrdersFeeCalculatorService.calculate_fee(sum)) }
    end

    # Calculating for this month

    def price_per_this_month
      Order.where("created_at >= ? AND created_at < ?",
                  Time.zone.now.beginning_of_month, Time.zone.now.end_of_month)\
           .group(:driver_id).sum(:price)
    end

    def fee_per_this_month
      price_per_this_month.transform_values { |sum| OrdersFeeCalculatorService.calculate_fee(sum) }
    end

    def salary_per_this_month
      price_per_this_month.transform_values { |sum| (sum - OrdersFeeCalculatorService.calculate_fee(sum)) }
    end

    # Calculating for last month

    def price_per_last_month
      Order.where("created_at >= ? AND created_at < ?",
                  (Time.zone.now - 1.month).beginning_of_month,
                  (Time.zone.now - 1.month).end_of_month).group(:driver_id).sum(:price)
    end

    def fee_per_last_month
      price_per_last_month.transform_values { |sum| OrdersFeeCalculatorService.calculate_fee(sum) }
    end

    def salary_per_last_month
      price_per_last_month.transform_values { |sum| (sum - OrdersFeeCalculatorService.calculate_fee(sum)) }
    end
  end

  private

  def send_order_finished_email_to_passenger
    OrderMailer.send_order_finished_email(self).deliver_now
  end
end
