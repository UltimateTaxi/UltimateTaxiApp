class Feedback < ApplicationRecord
  belongs_to :passenger
  belongs_to :driver
  belongs_to :order

  validates :rating, presence: true
  validates :text, presence: true, length: { maximum: 500 }
  validates :passenger_id, presence: true
  validates :driver_id, presence: true
end
