class Passenger < User
  has_many :orders, dependent: :nullify
  has_many :feedbacks, dependent: :nullify
end
