document.addEventListener("turbolinks:load", function () {
  var options = {
    url: function(phrase) {
      return "/passengers/geocode_address?address=" + phrase;
    },

    theme: "plate-dark",
    requestDelay: 1000
  };

  $("#starting_address").easyAutocomplete(options);
  $("#destination_address").easyAutocomplete(options);

  $("#starting_address, #destination_address, #order_tariff_id").on("change", function() {
    starting_address = $('#starting_address').val();
    destination_address = $('#destination_address').val();
    tariff = $('#order_tariff_id').val();

    starting_address_lat = Number(starting_address.split(",").splice(-2)[0]);
    starting_address_lon = Number(starting_address.split(",").splice(-2)[1]);

    destination_address_lat = Number(destination_address.split(",").splice(-2)[0]);
    destination_address_lon = Number(destination_address.split(",").splice(-2)[1]);

    renderMarker(starting_address_lat, starting_address_lon, destination_address_lat, destination_address_lon);
    renderLine(starting_address_lat, starting_address_lon, destination_address_lat, destination_address_lon);

    if (starting_address.length && destination_address.length && tariff.length >= 1) {
      $.get({
        url: "/passengers/calculate_price",
        data: {
          starting_address: starting_address,
          destination_address: destination_address,
          tariff: tariff
        },
        success: function(price) {
          return $("#price").val(Number(price));
        }
      });
    }
  })
});

starting_address_marker = null
destination_address_marker = null
line = null

window.initMap = function() {
  map = new google.maps.Map(document.getElementById("map"), {
    center: { lat: 48.92271703972721, lng: 24.71039887744783 },
    zoom: 13,
  });
}


window.renderMarker = function(starting_address_lat, starting_address_lon, destination_address_lat, destination_address_lon) {
  if (starting_address_lat && starting_address_lon >= 4) {
    if (starting_address_marker) { starting_address_marker.setMap(null) }
    starting_address_coordinates = new google.maps.LatLng(starting_address_lat, starting_address_lon);

    starting_address_marker = new google.maps.Marker({
      position: starting_address_coordinates,
      label: 'A',
      map: map
    });
  }

  if (destination_address_lat && destination_address_lon >= 4) {
    destination_address_coordinates = new google.maps.LatLng(destination_address_lat, destination_address_lon);

    if (destination_address_marker) { destination_address_marker.setMap(null) }
    destination_address_marker = new google.maps.Marker({
      position: destination_address_coordinates,
      label: 'B',
      map: map
    });
  }
}

window.renderLine = function(starting_address_lat, starting_address_lon, destination_address_lat, destination_address_lon) {
  if (starting_address_lat && starting_address_lon && destination_address_lat && destination_address_lon >= 4) {
    if (line) { line.setMap(null) }
    line = new google.maps.Polyline({
      path: [
          new google.maps.LatLng(starting_address_lat, starting_address_lon),
          new google.maps.LatLng(destination_address_lat, destination_address_lon)
      ],
      strokeColor: "#FF0000",
      strokeOpacity: 1.0,
      strokeWeight: 3,
      map: map
    });
  }
}
