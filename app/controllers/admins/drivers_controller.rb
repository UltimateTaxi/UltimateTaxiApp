module Admins
  class DriversController < Admins::ApplicationController
    def statistic
      @drivers = Driver.all
      @drivers_data = DriversTotalFetcherService.fetch
    end
  end
end
