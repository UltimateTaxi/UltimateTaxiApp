module Passengers
  class FeedbacksController < PassengersController
    def create
      @feedback = Feedback.create(order_id: params[:order_id])

      if @feedback.save
        redirect_to [:passengers, @order], notice: 'Feedback was successfully created.'
      else
        redirect_to [:passengers, @order], notice: 'Something was wrong.'
      end
    end
  end
end
