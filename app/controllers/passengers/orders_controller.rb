module Passengers
  class OrdersController < PassengersController
    def index
      @orders = Order.where(passenger_id: current_user.id)
    end

    def show
      @order = Order.find(params[:id])

      respond_to do |format|
        format.html
        format.pdf { render_pdf }
      end
    end

    def new
      @order = Order.new
    end

    def create
      @order                     = Order.new(order_params)
      @order.passenger           = current_user
      convert_address_to_coordinates

      if @order.save
        redirect_to [:passengers, @order], notice: 'Order was successfully created.'
      else
        render :new
      end
    end

    def geocode_address
      render json: AddressGeocoderService.new(params[:address]).geocode.to_json
    end

    def calculate_price
      render json: CalculatePriceService.new(params[:starting_address],
                                             params[:destination_address],
                                             params[:tariff]).calculate.to_json
    end

    private

    def convert_address_to_coordinates
      @order.starting_address    = order_params[:starting_address].split(',').last(2).map(&:to_f)
      @order.destination_address = order_params[:destination_address].split(',').last(2).map(&:to_f)
    end

    def render_pdf
      render pdf: "Receipt",
             page_width: '8in',
             page_height: '5in',
             template: "passengers/orders/receipt_pdf.html.haml",
             layout: "receipt_pdf.html.haml",
             orientation: "Landscape",
             lowquality: true, zoom: 1,
             dpi: 75, encoding: 'utf8'
    end

    def order_params
      params.require(:order).permit(:passenger_id, :driver_id, :starting_address, :destination_address,
                                    :tariff_id, :price, :state)
    end
  end
end
