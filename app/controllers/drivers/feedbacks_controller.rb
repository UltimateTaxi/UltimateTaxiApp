module Drivers
  class FeedbacksController < DriversController
    def create
      @order    = Order.find(params[:order_id])
      @feedback = @order.feedbacks.create(feedback_params)

      if @feedback.save
        redirect_to [:drivers, @order], notice: 'Feedback was successfully created.'
      else
        redirect_to [:drivers, @order], notice: 'Something was wrong.'
      end
    end

    private

    def feedback_params
      params.require(:feedback).permit(:rating, :text, :passenger_id, :driver_id)
    end
  end
end
