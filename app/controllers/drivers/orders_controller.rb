module Drivers
  class OrdersController < DriversController
    before_action :find_order, only: %i[show edit update change_order_status]

    def index
      @orders = Order.where(tariff_id: current_user.tariff_id).reverse
    end

    def show; end

    def edit; end

    def change_order_status
      @order.driver_id = current_user.id if @order.state == Order::CREATED_STATE
      @order.change_status
      redirect_to drivers_order_path
    end

    def update
      if @order.update(order_params)
        redirect_to @order, notice: 'Order was successfully updated.'
      else
        render :edit
      end
    end

    private

    def find_order
      @order = Order.find(params[:id])
    end

    def order_params
      params.require(:order).permit(:passenger_id, :driver_id, :starting_address, :destination_address,
                                    :tariff_id, :price, :state)
    end
  end
end
