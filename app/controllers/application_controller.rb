class ApplicationController < ActionController::Base
  layout :choose_layout

  def after_sign_in_path_for(_resource)
    current_user.admin? ? admins_root_path : home_path
  end

  def choose_layout
    if !user_signed_in?
      'application'
    elsif current_user.driver?
      'driver'
    elsif current_user.passenger?
      'passenger'
    end
  end

  add_flash_types :danger, :info, :warning, :success
end
