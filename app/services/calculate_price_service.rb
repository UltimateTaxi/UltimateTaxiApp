class CalculatePriceService
  attr_reader :starting_address, :destination_address, :tariff

  def initialize(starting_address, destination_address, tariff)
    @starting_address    = starting_address.split(',').last(2)
    @destination_address = destination_address.split(',').last(2)
    @tariff              = Tariff.find(tariff).cost
  end

  def calculate
    distance = Geocoder::Calculations.distance_between(starting_address, destination_address)
    (distance * tariff).round(2)
  end
end
