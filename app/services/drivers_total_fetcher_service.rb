class DriversTotalFetcherService
  class << self
    # rubocop:disable Metrics/MethodLength
    def fetch
      {
        total_price: Order.total_price,
        price_per_this_month: Order.price_per_this_month,
        price_per_last_month: Order.price_per_last_month,
        total_prices_fee: Order.total_prices_fee,
        fee_per_this_month: Order.fee_per_this_month,
        fee_per_last_month: Order.fee_per_last_month,
        total_price_salary: Order.total_price_salary,
        salary_per_this_month: Order.salary_per_this_month,
        salary_per_last_month: Order.salary_per_last_month
      }
    end
    # rubocop:enable Metrics/MethodLength
  end
end
