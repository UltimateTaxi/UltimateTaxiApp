class AddressGeocoderService
  attr_reader :address

  def initialize(address)
    @address = address
  end

  def geocode
    @results = Geocoder.search("Ivano-Frankivsk, #{address}")
    @results.map { |g| [g.display_name, g.coordinates] }
  end
end
