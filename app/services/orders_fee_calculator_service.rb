class OrdersFeeCalculatorService
  attr_reader :sum

  def self.calculate_fee(sum)
    (sum * Order::ORDERS_FEE).to_d
  end
end
