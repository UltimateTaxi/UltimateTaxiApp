class CreateTariffs < ActiveRecord::Migration[6.1]
  def change
    create_table :tariffs do |t|
      t.string :name, limit: 30, default: "", null: false
      t.decimal :cost

      t.timestamps
    end
  end
end
