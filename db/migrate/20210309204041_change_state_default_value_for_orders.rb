class ChangeStateDefaultValueForOrders < ActiveRecord::Migration[6.1]
  def change
    remove_column :orders, :passenger_id
    change_column :orders, :driver_id, :bigint
    add_reference :orders, :passenger, index: true
    change_column :orders, :state, :string, default: "created"
  end
end
