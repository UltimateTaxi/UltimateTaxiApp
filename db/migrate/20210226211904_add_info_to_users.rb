class AddInfoToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :first_name, :string, limit: 50, default: "", null: false
    add_column :users, :last_name, :string, limit: 50, default: "", null: false
    add_column :users, :type, :string, limit: 15, default: "", null: false
    add_column :users, :phone, :string, limit: 13, default: ""
    add_column :users, :tariff, :integer
    change_column :users, :email, :string, limit: 254, null: false, unique: true
  end
end
