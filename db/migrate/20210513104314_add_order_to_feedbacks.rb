class AddOrderToFeedbacks < ActiveRecord::Migration[6.1]
  def change
    add_reference :feedbacks, :passenger, index: true
    add_reference :feedbacks, :driver, index: true
    add_reference :feedbacks, :order, index: true
  end
end
