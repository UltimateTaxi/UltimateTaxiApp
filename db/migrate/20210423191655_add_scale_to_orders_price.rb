class AddScaleToOrdersPrice < ActiveRecord::Migration[6.1]
  def up
    change_table :orders do |t|
      t.change :price, :decimal, precision: 10, scale: 2
    end
  end

  def down
    change_table :orders do |t|
      t.change :price, :decimal
    end
  end
end
