class CreateOrders < ActiveRecord::Migration[6.1]
  def change
    create_table :orders do |t|
      t.integer :driver_id
      t.integer :passenger_id
      t.string  :starting_address, default: "", null: false
      t.string  :destination_address, default: "", null: false
      t.decimal :price
      t.string  :state, default: "Created"
      t.belongs_to :tariff, foreign_key: true

      t.timestamps
    end
  end
end
