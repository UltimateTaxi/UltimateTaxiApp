class AddTariffToUsers < ActiveRecord::Migration[6.1]
  def change
    remove_column :users, :tariff
    add_reference :users, :tariff, index: true
  end
end
