# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_05_13_104314) do

  create_table "feedbacks", charset: "utf8mb3", force: :cascade do |t|
    t.integer "rating"
    t.text "text"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "passenger_id"
    t.bigint "driver_id"
    t.bigint "order_id"
    t.index ["driver_id"], name: "index_feedbacks_on_driver_id"
    t.index ["order_id"], name: "index_feedbacks_on_order_id"
    t.index ["passenger_id"], name: "index_feedbacks_on_passenger_id"
  end

  create_table "orders", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "driver_id"
    t.string "starting_address", default: "", null: false
    t.string "destination_address", default: "", null: false
    t.decimal "price", precision: 10, scale: 2
    t.string "state", default: "created"
    t.bigint "tariff_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "passenger_id"
    t.index ["passenger_id"], name: "index_orders_on_passenger_id"
    t.index ["tariff_id"], name: "index_orders_on_tariff_id"
  end

  create_table "promocodes", charset: "utf8mb3", force: :cascade do |t|
    t.decimal "discount", precision: 10, scale: 2
    t.datetime "expire_date"
    t.integer "usage_count"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "name"
  end

  create_table "tariffs", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "name", limit: 30, default: "", null: false
    t.decimal "cost", precision: 10
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "users", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "email", limit: 254, default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "first_name", limit: 50, default: "", null: false
    t.string "last_name", limit: 50, default: "", null: false
    t.string "type", limit: 15, default: "", null: false
    t.string "phone", limit: 13, default: ""
    t.bigint "tariff_id"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["tariff_id"], name: "index_users_on_tariff_id"
  end

  add_foreign_key "orders", "tariffs"
end
